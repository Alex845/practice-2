
import Vuex from 'vuex'
import Vue from 'vue'
import createPersistedState from 'vuex-persistedstate'



Vue.use(Vuex)

const store = new Vuex.Store({
    plugins: [createPersistedState()],
   state: {
     count: 0,
     color:0,
     user:0
   },
   mutations: {
       setCount: (state, count) => state.count = count,
       back:(state,style)=>state.color=style,
       setUser:(state, user)=>state.user=user,
   },
   getters: {
       getCount: (state) =>
       {
           return state.count
       },
       getback:(state)=>
       {
            return state.color
        },
        getUser:(state)=>{
            return state.user
        } 
    }
})
export default store;
