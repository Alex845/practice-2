import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'


import App from './components/App.vue'
import students from './components/students.vue'
import studentInfo from './components/studentInfo.vue'
import login from './components/login.vue'
import store from './store.js'


const routes=[
   {path: '/', component:students, meta:{requiresAuth:true}},
   {path: '/student-info/:id', component: studentInfo, props:true, meta:{requiresAuth:true}},
   {path: '/login', component:login},
]

const router = new VueRouter({
   routes
})

router.beforeEach((to, from, next) => {
   if (to.matched.some(record => record.meta.requiresAuth)){
      if(store.getters.getUser===null){
         next({
            path:'/login',
            query:{redirect: to.fullPath}
         })
      }  else {
         next()
      }
   }else{
      next()
   }  

})

Vue.use(VueRouter)


new Vue({
   render: h => h(App),
   el: '#app',
   router,
   store
})
